Name:           git-toolbelt
Version:        1.5.0
Release:        1%{?dist}
Summary:        Helper tools to make everyday life with Git much easier

License:        BSD
URL:            https://github.com/nvie/%{name}
Source0:        https://github.com/nvie/%{name}/archive/v%{version}.tar.gz
Source1:        %{name}-readme2man

Requires:       bash
Requires:       git
Requires:       fzf

BuildRequires:  python3
BuildRequires:  pandoc

BuildArch:      noarch

%description
Helper tools to make everyday life with Git much easier.

Everyday helpful commands:
    git-cleanup
    git-cleanup-squashed
    git-current-branch
    git-main-branch
    git-fixup
    git-fixup-with
    git-active-branches
    git-local-branches
    git-local-commits
    git-merged / git-unmerged / git-merge-status
    git-branches-containing
    git-recent-branches
    git-remote-branches
    git-remote-tracking-branch
    git-repo
    git-root
    git-initial-commit
    git-sha
    git-stage-all
    git-unstage-all
    git-update-all
    git-workon
    git-modified
    git-modified-since
    git-separator
    git-spinoff

Statistics:
    git-committer-info

Commands to help novices out:
    git-drop-local-changes
    git-stash-everything
    git-push-current
    git-undo-commit
    git-undo-merge

Commands that simplify scripting (these commands typically only return
exit codes and have no output):
    git-is-repo
    git-is-headless
    git-has-local-changes / git-is-clean / git-is-dirty
    git-has-local-commits
    git-contains / git is-ancestor
    git-local-branch-exists
    git-remote-branch-exists
    git-tag-exists

Advanced usage:
    git-skip / git-unskip / git-show-skipped
    git-commit-to
    git-cherry-pick-to
    git-delouse
    git-shatter-by-file

%prep
%setup -q
cp -p %{SOURCE1} .

%build
mkdir -p mans && ./%{name}-readme2man %{version}

%install
mkdir -p %{buildroot}/%{_bindir}
install -m 0755 git-* %{buildroot}/%{_bindir}/
mkdir -p %{buildroot}/%{_mandir}/man1/
install -m 0644 mans/git-*.1 %{buildroot}/%{_mandir}/man1/

%files
%license LICENSE
%doc PUBLISHING.md README.md
%{_bindir}/git-*
%{_mandir}/man1/git-*.1.gz

%changelog
* Sat Sep 12 2020 Evgeny Kolesnikov <evgenyz@gmail.com> - 1.5.0-1
- Initial package (v1.5.0)

