# SPDX-License-Identifier: MIT
%global forgeurl    https://github.com/evilmartians/mono
%global forgesource martian-mono-%{version}-otf.zip
Version:            1.0.0
%forgemeta

Release: 1%{?dist}
URL:     https://github.com/evilmartians/mono

%global foundry           Martian
%global fontlicense       OFL 1.1
#global fontlicenses      OFL.txt
#global fontdocs          *.md

%global common_description %{expand:
Martian Mono is a monospaced version of the Martian Grotesk font for code style
design. It inherits Grotesk’s brutal and eye-catching aesthetics as well as
all of its benefits—metrics equilibrium, readability and intelligibility,
and convenience for web developers and designers who believe in
a systematic approach to design.}

%global fontfamily       Martian Mono
%global fontsummary      A mono-space font family containing coding ligatures
%global fonts            *.otf
%global fontconfngs      %{SOURCE10}

Source0:  %{forgesource}
Source10:  %{fontpkgname}.xml

%fontpkg

#fontmetapkg

%prep
%setup -q -c -T -a 0

%build
%fontbuild

%install
%fontinstall

%check
%fontcheck

%fontfiles

%changelog
* Thu Jan 12 2023 Evgeny Kolesnikov <evgenyz@gmail.com> - 1.0.0-1
- Initial packaging
